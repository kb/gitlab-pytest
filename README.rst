Docker Container for GitLab CI
******************************

Creating this Docker image is one of the steps for setting up:

GitLab Multi-Runner and Docker
https://www.kbsoftware.co.uk/docs/dev-gitlab.html#gitlab-multi-runner-and-docker

.. tip:: I had to ``sudo -i`` to run the ``docker`` commands...

Build::

  docker build -t pkimber/gitlab-pytest .

.. note:: To do a clean build,
          docker build --pull --no-cache --tag pkimber/gitlab-pytest .

Check the size of the image::

  docker image list

Create a container from the image::

  docker run -t -i pkimber/gitlab-pytest /bin/bash

Log into Docker hub::

  docker login

Push to Docker hub::

  docker push pkimber/gitlab-pytest

Issues
======

If your ``apt`` repository seems out of date (e.g. *Failed to fetch*), then
you can do a clean build of the container::

  docker build --no-cache -t pkimber/gitlab-pytest .
