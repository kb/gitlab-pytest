FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London

# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
RUN apt-get clean && apt-get update && apt-get install -y \
    build-essential \
    default-libmysqlclient-dev \
    git \
    libpq-dev \
    locales \
    postgresql-client \
    python3-dev \
    python3.10-venv \
    tzdata \
    unixodbc-dev \
    --no-install-recommends --no-install-suggests \
&& rm -rf /var/lib/apt/lists/*

# Old Ubuntu 18.04 (for reference)
# RUN apt-get install -y libjpeg8-dev libpq-dev libxml2-dev libxslt-dev
# RUN apt-get install -y git libmysqlclient-dev postgresql-client tdsodbc
# RUN apt-get install -y curl
# # https://github.com/mkleehammer/pyodbc/wiki/Install
# # https://www.kbsoftware.co.uk/crm/ticket/4957/
# RUN apt-get install -y unixodbc-dev
# RUN DEBIAN_FRONTEND=noninteractive TZ=Europe/London apt-get -y install tzdata

# locale
RUN locale-gen en_GB.UTF-8
ENV LANG en_GB.UTF-8
ENV LANGUAGE en_GB:en
ENV LC_ALL en_GB.UTF-8

# Old Ubuntu 18.04 (for reference)
# # rust for cryptography for mozilla-django-oidc
# RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
# ENV PATH="/root/.cargo/bin:${PATH}"
